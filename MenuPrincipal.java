import javax.swing.JOptionPane;

public class MenuPrincipal
{
	Computadora computadora;
	
	public MenuPrincipal()
	{
		menu();
	}//FinMenu
	public void menu()
	{
		char opcion;
		do
		{
			opcion=(JOptionPane.showInputDialog("******** Menu principal ************\n"
		+"\na. Crear instancia de computadora sin valores."
		+"\nb. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos"
		+"\nc. Modificar la placa de la computadora."
		+"\nd. Modificar el modelo de la computadora."
		+"\ne. Modificar el tamaño de la pantalla de la computadora."
		+"\nf. Modificar el estado de la computadora."
		+"\ng. Ver los datos de la computadora."
		+"\nh. Salir.")).charAt(0);
		
		switch (opcion)
		{
			case 'a':
				computadora= new Computadora();
			break;
			
			case 'b':
				String modelo="";
				String placa=""; 
				String estado="";
				int pantalla=0;
				
				modelo=JOptionPane.showInputDialog ("Digite el modelo de la computadora");
				placa=JOptionPane.showInputDialog ("Digite la placa de la computadora");
				estado=JOptionPane.showInputDialog ("Digite el estado en el que se encuentra la computadora");
				pantalla=Integer.parseInt(JOptionPane.showInputDialog("Digite las pulgadas de la pantalla de la computadora"));
				
				computadora=new Computadora(modelo, placa, pantalla);
				computadora.setEstado(estado);	
			break;
			
			case 'c':
				if (computadora!=null)
				{int opcionMP;
				opcionMP=0;
				opcionMP=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar el estado de la computadora?","Modificar",JOptionPane.YES_NO_OPTION);
				if (opcionMP==0){
					placa=JOptionPane.showInputDialog ("Digite la placa de la computadora");
					computadora.setPlaca(placa);
				}//Fin del if
				JOptionPane.showMessageDialog (null, "La placa de la computadora ha sido modificado");
				}else{JOptionPane.showMessageDialog(null, "No ha creado una instancia");}
			break;
			
			case 'd':
				if (computadora!=null)
				{int opcionMM;
				opcionMM=0;
				opcionMM=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar el estado de la computadora?","Modificar",JOptionPane.YES_NO_OPTION);
				if (opcionMM==0){
					modelo=JOptionPane.showInputDialog ("Digite el modelo de la computadora");
					computadora.setModelo(modelo);
				}//Fin del if
				JOptionPane.showMessageDialog (null, "El tamaño de la pantalla de la computadora ha sido modificado");}else{JOptionPane.showMessageDialog(null, "No ha creado una instancia");}
			break;
			
			case 'e':
				if (computadora!=null)
				{int opcionMT;
				opcionMT=0;
				opcionMT=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar el estado de la computadora?","Modificar",JOptionPane.YES_NO_OPTION);
				if (opcionMT==0){
					pantalla=Integer.parseInt(JOptionPane.showInputDialog ("Digite el tamaño de la pantalla de la computadora"));
					computadora.setPantalla(pantalla);
				}//Fin del if
				JOptionPane.showMessageDialog (null, "El tamaño de la pantalla de la computadora ha sido modificado");}else{JOptionPane.showMessageDialog(null, "No ha creado una instancia");}
			break;
			
			case 'f':
				if (computadora!=null)
				{int opcionME;
				opcionME=0;
				opcionME=JOptionPane.showConfirmDialog(null,"¿Seguro que desea modificar el estado de la computadora?","Modificar",JOptionPane.YES_NO_OPTION);
				if (opcionME==0){
					estado=JOptionPane.showInputDialog ("Digite el peso del cliente:");
					computadora.setEstado(estado);
				}//Fin del if
				JOptionPane.showMessageDialog (null, "El estado de la computadora ha sido modificado");}else{JOptionPane.showMessageDialog(null, "No ha creado una instancia");}
			break;
			
			case 'g':JOptionPane.showMessageDialog (null, "Los datos de la computadora son los siguientes:"+"\n"+ computadora.toString ());
			break;
			
			case 'h':JOptionPane.showMessageDialog (null,"Nos vemos, regresa pronto");
			break;
			
			default: {JOptionPane.showMessageDialog (null,"Esa opcion no es valida");}
		}//Fin del switch
		}while (opcion!='h');
	}//Fin menu
	public static void main (String arg[])
	{
		MenuPrincipal menuPrincipal= new MenuPrincipal();
	}//Fin del main
}//Fin de la clase MenuPrincipal
